package com.hjh.demo.rabbitmq.model.service.simple;

import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;
 
@Component
@RabbitListener(queues = "hello")
public class HelloReceiver1 {

  @RabbitHandler
  public void process1(String hello) {
    System.out.println("Receiver1 : " + hello);
  }
} 