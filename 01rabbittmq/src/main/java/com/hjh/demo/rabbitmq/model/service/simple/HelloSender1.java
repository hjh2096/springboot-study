package com.hjh.demo.rabbitmq.model.service.simple;

import java.util.Date;

import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Slf4j
@Component
public class HelloSender1 {

    @Autowired
    private RabbitTemplate rabbitTemplate;

    public void send() {
        String sendMsg = "hello1 " + new Date();
        log.info("==========>sendMsg:{}",sendMsg);

        //第一个参数是routingKey(没有指定exchange时,routingKey直接对应队列名称)
        //第二个参数是发送的数据
        this.rabbitTemplate.convertAndSend("hello", sendMsg);
    }

}
 
  